window.browser = window.browser || window.chrome;
var theme = '';

// ---------------------
// Check and apply theme
// ---------------------
browser.storage.local.get('themeJSON').then(function(theme) {
	return gotTheme(theme);
}).catch((error) => {
  console.error(error);
});

function gotTheme(theme){
  theme = JSON.parse(`${theme.themeJSON}`);

  if (theme == 'dark'){
    darken();
  }

  if (theme == 'light'){
    lighten();
  }
}

// Only apply the system's theme if the user hasn't specifically set the light
// theme for Text Encryptor.
if (window.matchMedia && !!window.matchMedia('(prefers-color-scheme: dark)').matches) {
  if (theme != 'light'){
    darken();
  }
}

// -------------
// Go to Mozilla
// -------------
$('.header-text').click(function(){
	browser.tabs.create({
		url: 'https://addons.mozilla.org/firefox/addon/text-encryptor'
	});
});

// -----------------
// User switches tab
// -----------------
$('.tab-button').click(function(){
  $('.tab-button').toggleClass('tab-button--inactive');
  $('.tab-button').toggleClass('tab-button--active');

  if ($(this).hasClass('tab-button-encrypt')) {
    switchTabTo('encrypt');
  }

  if ($(this).hasClass('tab-button-decrypt')) {
    switchTabTo('decrypt');
  }
});

// ----------------------------
// User enters text or password
// ----------------------------
$('.input').on('input propertychange paste', function(){
  validateInput($(this));
});

$('.input').keyup(function(){
  validateInput($(this));
});

// -------------------------------
// User clicks on input's eye icon
// -------------------------------
$('.show-password').click(function(){
  $(this).toggleClass('show-password--inactive');
  $(this).toggleClass('show-password--active');

  /*$(this).find('i').toggleClass('fa-eye');
  $(this).find('i').toggleClass('fa-eye-slash');*/

  if ($('.input-password').attr('type') == 'password'){
    $('.input-password').attr('type', 'text');
  } else {
    $('.input-password').attr('type', 'password');
  }
});

// -------------------------------------
// User clicks on Encrypt/Decrypt button
// -------------------------------------
$('.button--encrypt, .button--decrypt').click(function(){
  var encryptionType = '';
  var cryptext = $('#cryptext').val();
  var password = $('#password').val();
  var cryptextIsValid = false;
  var passwordIsValid = false;
  var output = '';

  if ($(this).hasClass('button--encrypt')){
    encryptionType = 'encrypt';
  }

  if ($(this).hasClass('button--decrypt')){
    encryptionType = 'decrypt';
  }

  cryptextIsValid = validateInput($('.input-text'));
  passwordIsValid = validateInput($('.input-password'));

  if (cryptextIsValid && passwordIsValid) {
    // Proceed with actual encryption/decryption process
    if (encryptionType == 'encrypt'){
      output = CryptoJS.AES.encrypt(cryptext, password).toString();
    }

    if (encryptionType == 'decrypt'){
      output = CryptoJS.AES.decrypt(cryptext, password).toString(CryptoJS.enc.Utf8);
    }

    $('.input-container').addClass('input-container--hidden');
    $('.output-container').removeClass('output-container--hidden');
    $('.output').val(output);

    $('main').removeClass('main--encrypt');
    $('main').removeClass('main--decrypt');
    $('main').addClass('main--done');
  }
});

// --------------------------
// User clicks on Done button
// --------------------------
$('.button--done').click(function(){
  var mode = '';

  if ($('.tab-button-encrypt').hasClass('tab-button--active')){
    mode = 'encrypt';
  } else {
    mode = 'decrypt';
  }

  switchTabTo(mode);
  cleanUp();
});

// --------------------------------
// User clicks on dark theme button
// --------------------------------
$('.theme-button--dark').click(function(){
  var theme = 'dark';
	var themeJSON = JSON.stringify(theme);
	browser.storage.local.set({ themeJSON });
  darken();
});

// ---------------------------------
// User clicks on light theme button
// ---------------------------------
$('.theme-button--light').click(function(){
  var theme = 'light';
	var themeJSON = JSON.stringify(theme);
	browser.storage.local.set({ themeJSON });
  lighten();
});

// ---------------------------
// Switching the tab's content
// ---------------------------
function switchTabTo(target){
  $('.input-container').removeClass('input-container--hidden');
  $('.output-container').addClass('output-container--hidden');

  if (target == 'encrypt'){
    $('main').addClass('main--encrypt');
    $('main').removeClass('main--decrypt');
  }

  if (target == 'decrypt'){
		$('main').addClass('main--decrypt');
    $('main').removeClass('main--encrypt');
  }

  $('main').removeClass('main--done');
}

// --------------------------
// Validate given input field
// --------------------------
function validateInput(element){
  var inputType = undefined;
  var inputContainer = undefined;
  var showPWicon = undefined;
  var inputLength = element.val().length;
  var valid = false;

  if (element.hasClass('input-text')){
    inputType = 'text';
		inputContainer = $('.input-container-text');
  }

  if (element.hasClass('input-password')){
    inputType = 'password';
		inputContainer = $('.input-container-password');
    showPWicon = $('.show-password');
  }

  if (inputLength == 0){
		$(inputContainer).removeClass('input-container--normal');
		$(inputContainer).removeClass('input-container--warning');
		$(inputContainer).addClass('input-container--error');
  }

  if (inputLength > 0) {
		$(inputContainer).removeClass('input-container--error');
		$(inputContainer).removeClass('input-container--warning');
		$(inputContainer).addClass('input-container--normal');

    valid = true;
  }

  // Iff password is between 0 and 10 characters, display “too short” warning.
  // Otherwise, hide warning.
  if (inputType == 'password'){
    if (inputLength > 0 && inputLength < 10){
			$(inputContainer).removeClass('input-container--error');
			$(inputContainer).removeClass('input-container--normal');
			$(inputContainer).addClass('input-container--warning');
    }
  }

  return valid;
}

// -------------------
// Clean up everything
// -------------------
function cleanUp(){
  $('.input').val('');

	$('.input-container-text, .input-container-password').removeClass('input-container--error');
	$('.input-container-text, .input-container-password').removeClass('input-container--warning');
	$('.input-container-text, .input-container-password').addClass('input-container--normal');
}

// -----------------
// Apply light theme
// -----------------
function lighten(){
  $('link[href="css/popup-dark.css"]').remove();
	$('body').removeClass('body--dark');
}

// ----------------
// Apply dark theme
// ----------------
function darken(){
  addCSS('css/popup-dark.css');
	$('body').addClass('body--dark');
}

function addCSS(filename){
	var head = document.getElementsByTagName('head')[0];
	var style = document.createElement('link');

	style.href = filename;
	style.type = 'text/css';
	style.rel = 'stylesheet';

	head.append(style);
}