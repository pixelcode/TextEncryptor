function localiseInnerText() {
  var elements = document.querySelectorAll("[i18n-content]");

  for (var i in elements)

    if (elements.hasOwnProperty(i)) {
      var obj = elements[i];
      var tag = obj.getAttribute("i18n-content").toString();
      var msg = browser.i18n.getMessage(tag);

      if (msg && msg !== tag) obj.textContent = msg;
    }
}

function localiseTitle() {
  var elements = document.querySelectorAll("[i18n-title]");

  for (var i in elements)

    if (elements.hasOwnProperty(i)) {
      var obj = elements[i];
      var tag = obj.getAttribute("i18n-title").toString();
      var msg = browser.i18n.getMessage(tag);

      if (msg && msg !== tag) obj.title = msg;
    }
}

function localisePlaceholder() {
  var elements = document.querySelectorAll("[i18n-placeholder]");

  for (var i in elements)

    if (elements.hasOwnProperty(i)) {
      var obj = elements[i];
      var tag = obj.getAttribute("i18n-placeholder").toString();
      var msg = browser.i18n.getMessage(tag);

      if (msg && msg !== tag) obj.placeholder = msg;
    }
}

function localiseAll(){
  localiseInnerText();
  localiseTitle();
  localisePlaceholder();
}

localiseAll();