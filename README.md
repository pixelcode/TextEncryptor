# Text Encryptor
Text Encryptor is a simple Firefox addon which lets you encrypt text securely and easily.

![](https://codeberg.org/pixelcode/TextEncryptor/raw/commit/443f053fe0a3f1cb1110d791ad61ea8c816a3b6b/assets/banner.png)

## Purpose
This addon encrypts your confidential text using AES, the most secure encryption algorithm in the world. No-one is able to crack it within a lifetime – not even the police or NSA!

Use cases
- encrypt your private notes
- send your banking details unmonitored to a friend who uses WhatsApp only *sigh*
- store your passwords on your PC without exposing them to hackers

Text Encryptor
- doesn't track you
- doesn't store, send or upload your inputs anywhere
- is ad-free
- is Free and Ethical software

## Screenshots
| Before encrypting | After encrypting | Dark mode |
| ----------------- | ---------------- | --------- |
| ![](https://codeberg.org/pixelcode/TextEncryptor/raw/commit/ce514464f68198d6ef4243d4ba1f1af650db7b71/assets/v1.7%20screenshot%20before%20encryption.png) | ![](https://codeberg.org/pixelcode/TextEncryptor/raw/commit/ce514464f68198d6ef4243d4ba1f1af650db7b71/assets/v1.7%20screenshot%20after%20encryption.png) | ![](https://codeberg.org/pixelcode/TextEncryptor/raw/commit/ce514464f68198d6ef4243d4ba1f1af650db7b71/assets/v1.7%20screenshot%20dark%20mode.png) |

[![](https://codeberg.org/pixelcode/TextEncryptor/raw/commit/93488370b9265435fd741385df04011cd426c978/get-the-addon.png)](https://addons.mozilla.org/firefox/addon/text-encryptor/?utm_source=codeberg&utm_medium=readme&utm_content=get-the-addon-button)

## Languages
[![Translation status](https://weblate.bubu1.eu/widgets/text-encryptor/-/text-encryptor/multi-auto.svg)](https://weblate.bubu1.eu/engage/text-encryptor/)

Thanks to @mondstern (translator-in-chief) for his many translations! ❤️

Do you speak another language fluently? Then consider translating **Text Encryptor** on [Weblate](https://weblate.bubu1.eu/projects/text-encryptor/text-encryptor/).

## How to encrypt text?
Enter your plaintext and choose a secure password. Then, click on "Encrypt".

## How to decrypt text?
Click on the "Decrypt" button above. Enter your ciphertext and the corresponding password used to encrypt it. Then, click on "Decrypt".

## I have forgotten the password
Sorry, it's not possible to recover your data without the correct password. If I wanted to add such a feature, this would destroy AES' security and give hackers the opportunity to easily crack your data.

## Why do my inputs disappear each time I close the popup?
If I wanted to let you keep your inputs I'd have to store them on your computer which is a big risk.

## "There was an error"
If **Text Encryptor** tells you that there was an error, there's something wrong with the data you've entered. Check for spelling mistakes and try it again.

## Where to report bugs?
Feel free to open a [new issue](https://codeberg.org/pixelcode/TextEncryptor/issues/new).

## May I contribute?
Sure! Pull requests fixing bugs and closing issues are always welcome.

## Rate Text Encryptor
Also, you can write a review on [Mozilla](https://addons.mozilla.org/firefox/addon/text-encryptor/reviews/?utm_source=codeberg&utm_medium=readme&utm_content=review-link) (you need a Firefox account).

## Where does the encryption algorithm come from?
[AES](https://de.wikipedia.org/wiki/Advanced_Encryption_Standard) was created by [Joan Daemen](https://de.wikipedia.org/wiki/Joan_Daemen) and [Vincent Rijmen](https://de.wikipedia.org/wiki/Vincent_Rijmen). [Evan Vosberg](https://github.com/brix) has implemented it in a JavaScript library called [CryptoJS](https://github.com/brix/crypto-js). Fortunately it is FOSS so that other programs like **Text Encryptor** can use it. Thanks, Evan!

## ⚠️ Dangers
### Insecure Passwords
**MOST IMPORTANTLY** you have to make sure you use **secure passwords** in order to encrypt your plaintext securely! **Insecure passwords can't protect your private data at all!**

**Secure passwords**
- are as long as possible, at least 30 characters
- consist of letters, digits, symbols and spaces
- contain real words as well as gibberish
- do **NOT** contain private details like forenames, lastnames, birthdates, pets' names, phone numbers etc. because hackers could just guess them for example by checking your social media profiles.

The fact that the weak password warning in the addon's popup disappears does not automatically mean that the password is strong!

Never ever use passwords like '12345' or 'pizza'!

### Compromised machine
Don't use **Text Encryptor** on a compromised computer (e.g. infected by a virus) because then the 'bad guys' can spy on you and get your private data before you encrypt it. AES can't protect your private data if hackers spy on your keyboard!

### Too sensitive data
Don't use **Text Encryptor** if your private data is so sensitive that its illegitimate decryption is a danger to your life or that of the public. Don't use **Text Encryptor** if you are a whistleblower. You may still use **Text Encryptor** if it should just **add** an **extra** layer of security to an already existing secure encryption. You just shouldn't *rely* on **Text Encryptor**.

#### So is Text Encryptor not secure after all?
Yes, it is secure due to AES. But there might be some so-called implementation errors (we programmers aren't perfect, you know) which is why you should always combine multiple encryption tools and algorithms.

##### So: Do use Text Encryptor as a part of several encryption layers but do not rely on it itself if you or your data are particularly vulnerable. Okay?

## Licence
**Text Encryptor** uses **[CrypoJS](https://github.com/brix/crypto-js)** created by [Evan Vosberg (brix)](https://github.com/brix). It may be used according to the [MIT license](https://opensource.org/licenses/MIT). Thank you, Evan!

**Text Enryptor** uses **[jQuery](https://jquery.com)** according to the [MIT Licence](https://github.com/jquery/jquery/blob/main/LICENSE.txt).

**Text Encryptor** uses **[Font Awesome](https://fontawesome.com)** icons according to the [Attribution 4.0 International (CC BY 4.0)](https://creativecommons.org/licenses/by/4.0/) license.

![](https://codeberg.org/ForGoodEyesOnly/for-good-eyes-only/raw/commit/342919c4507e0832d20501c4fff422b12119621b/assets/banner/Banner%20250.png)

You may use **Text Encryptor's** source code according to the [For Good Eyes Only Licence v0.2](https://codeberg.org/pixelcode/TextEncryptor/src/branch/master/For%20Good%20Eyes%20Only%20Licence%20v0.2.pdf). Do not re-use TextEncryptor's name, logos, icons, banners and previews.

All versions of Text Encryptor are licensed under v0.2, which replaces v0.1 under which Text Encryptor was previously licensed. v0.1 is therefore void for any versions of Text Encryptor.

[![](https://codeberg.org/ForGoodEyesOnly/for-good-eyes-only/raw/branch/master/assets/summary/Summary%20v0.2%20750px.png)](https://codeberg.org/ForGoodEyesOnly/for-good-eyes-only#licence-summary)